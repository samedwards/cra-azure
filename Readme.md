## Stack

- Frontend Framework: React / Gatsby
- CMS: GraphCMS
- Build pipeline: Gitlab CI
- Hosting: Azure static website

## Quick Start

```
yarn install
yarn develop
```

Visit http://localhost:8000

## Live preview

The draft edits on the CMS environment can be previewed by visiting the path `/preview/venue?preview=generic-coffee-shop`

## URL
Visit site at https://testaccountsedwards.z33.web.core.windows.net/