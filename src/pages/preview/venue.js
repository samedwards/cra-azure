import React, {useEffect, useState} from 'react';
import Layout from "../../components/layout"

const VenuePage = (props) => {
  const [data, setData] = useState(undefined)
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  useEffect(() => {
    const query = async () => {
      const previewQueryString = `${new URLSearchParams(props.location.search).get('preview')}`
      const accessToken = `${new URLSearchParams(props.location.search).get('token')}`

      if (!accessToken || accessToken === 'null') {
        setError(true)
        setErrorMessage('Error: No access token in query string')
        return
      }

      if (!previewQueryString || previewQueryString === 'null') {
        return
      }
      try {
        const result = await fetch("https://api-eu-central-1.graphcms.com/v2/ckiorqoy30s5y01z12oon8ubh/master", {
          "headers": {
            "accept": "*/*",
            "Authorization": `Bearer ${accessToken}`,
            "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
            "content-type": "application/json",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
          },
          "referrer": "http://localhost:8000/",
          "referrerPolicy": "strict-origin-when-cross-origin",
          "body": JSON.stringify({
            operationName: 'VenuePageQuery',
            variables: { slug: previewQueryString},
            query: `
            query VenuePageQuery ($slug: String!) {
              venue(where: {slug: $slug}, stage: DRAFT) {
                id
                title
                description
                photo {
                  url
                }
                accessibilityFeatures {
                  title,
                  id
                }
              }
            }
          `
          }),
          "method": "POST",
          "mode": "cors",
          "credentials": "omit"
        });
        const data = await result.json()
        if (data.data.error) {
          setError(true)
          return
        }
        setData(data.data)
      } catch (e) {
        setError(true)
      }
  };
  query()
  },[])


  if (error) {
    return (
      <Layout>
        <p>{errorMessage || `Error: please refer to console`}</p>
      </Layout>
    )
  }
  if (!data) {
    return (
      <Layout>
        <p>Loading Preview</p>
      </Layout>
    )
  }
  return (
    <Layout>
    <h1>{data.venue.title}</h1>
    <p>{data.venue.description}</p>
    <img src={data.venue.photo.url}></img>
    <p>Accessibility Features</p>
    <ul>
      {data.venue.accessibilityFeatures && data.venue.accessibilityFeatures.map(feature => (
        <li key={feature.id}>{feature.title}</li>
      ))}
    </ul>
  </Layout>
);
}

export default VenuePage;