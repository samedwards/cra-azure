import React from "react"
import { graphql } from "gatsby"
import { Link } from "gatsby"


import Layout from "../components/layout"


export default function Home ({ data: { values } }) {
  return (
    <Layout>
      <h1>Location List</h1>
      <ul>
      {values.venues.map(venue => (
        <li key={venue.id}>
          <Link to={`venues/${venue.slug}`}>{venue.title}</Link>
        </li>
      ))}
      </ul>
    </Layout>
  )
}



export const pageQuery = graphql`
  query HomePageQuery {
    values: cms {
      venues {
        id
        slug
        title
      }
    }
  }
`
