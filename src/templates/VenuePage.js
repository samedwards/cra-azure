import React from 'react';
import { graphql } from 'gatsby';
import Layout from "../components/layout"

const VenuePage = ({ data: { value: {venue: venue}  } }) => {
  return (
    <Layout>
    <h1>{venue.title}</h1>
    <p>{venue.description}</p>
    <img src={venue.photo.url}></img>
    <p>Accessibility Features</p>
    <ul>
      {venue.accessibilityFeatures && venue.accessibilityFeatures.map(feature => (
        <li key={feature.id}>{feature.title}</li>
      ))}
    </ul>
  </Layout>
);
}

export const pageQuery = graphql`
  query VenuePageQuery ($id: ID!) {
    value: cms {
      venue(where: {id: $id}) {
        id
        title
        description
        photo {
          url
        }
        accessibilityFeatures {
          title,
          id
        }
      }
    }
  }
`;

export default VenuePage;