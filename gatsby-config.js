require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const graphqlOptions = {
  typeName: 'CMS',
  fieldName: 'cms',
  url: process.env.GRAPHCMS_ENDPOINT,
  headers: {
    Authorization: `Bearer ${process.env.GRAPHCMS_TOKEN}`
  }
};

module.exports = {
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Location`,
        short_name: `Location`,
        start_url: `/`,
        background_color: `#FFF`,
        theme_color: `#FAE042`,
        display: `standalone`,
        icon: `src/images/icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        workboxConfig: {
          runtimeCaching: [
            {
              urlPattern: /^https?:\/\/media\.graphcms\.com.*/,
              handler: `StaleWhileRevalidate`,
            },
          ]
        }
      }
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: 'gatsby-source-graphql',
      options: graphqlOptions,
    },
  ],
};