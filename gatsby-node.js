exports.createPages = async ({ graphql, actions: { createPage } }) => {
  const {
    data: { values },
  } = await graphql(`
    {
      values: cms {
        venues {
          id
          slug
        }
      }
    }
  `);

  values.venues.forEach(({ id, slug }) =>
    createPage({
      path: `/venues/${slug}`,
      component: require.resolve(`./src/templates/VenuePage.js`),
      context: {
        id,
      },
    })
  );
}


// };